require 'tk'
require 'dotenv'
Dotenv.load
require './http-service.rb'

$CITY_NAME = ENV['CITY_NAME']

$httpService = HTTPService.new

def main()
  $root = TkRoot.new { title 'Weather' }

  TkLabel.new($root) do
    text 'Weather'
    pack { padx 14; pady 14; side ‘left’ }
  end

  button = TkButton.new {
     text "Get Weather for #{$CITY_NAME}"
     command proc { getWeather() }
     pack
  }
  button.configure('activebackground', 'blue')

  Tk.mainloop
end

def getWeather()
  data = $httpService.getCurrentWeather($CITY_NAME)
  updateCoordinates(data)
  updateWeather(data)
  updateMain(data)
end

def updateMain(data)
  main = data['main']

  temp = kelvinToF(main['temp'])
  feels = kelvinToF(main['feels_like'])
  min = kelvinToF(main['temp_min'])
  max = kelvinToF(main['temp_max'])
  
  coor = TkLabel.new($root) do
    text "Min: #{min.to_i}F\nAverage: #{temp.to_i}F\nFeels Like: #{feels.to_i}F\nMax: #{max.to_i}F"
    borderwidth 5
    font TkFont.new('times 20 bold')
    foreground  "red"
    relief      "groove"
    pack("side" => "right",  "padx"=> "50", "pady"=> "50")
  end
end

def kelvinToCelsius(temp)
  return temp - 273.15
end

def kelvinToF(temp)
  cel = kelvinToCelsius(temp)
  f = cel * 1.8
  return f + 32
end
  
def updateCoordinates(data)
  coord = data['coord']
  lat = coord['lat']
  lon = coord['lon']
  coor = TkLabel.new($root) do
    text "Coordinates: #{lat}, #{lon}"
    borderwidth 5
    font TkFont.new('times 20 bold')
    foreground  "red"
    relief      "groove"
    pack("side" => "right",  "padx"=> "50", "pady"=> "50")
  end
end

def updateWeather(data)
  weather = data['weather'][0]
  main = weather['main']
  desc = weather['description']

  coor = TkLabel.new($root) do
    text "#{main}: #{desc}"
    borderwidth 5
    font TkFont.new('times 20 bold')
    foreground  "red"
    relief      "groove"
    pack("side" => "right",  "padx"=> "50", "pady"=> "50")
  end

end
  

main()