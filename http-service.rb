require 'httparty'

class HTTPService
  def initialize
    @appId = ENV['APP_ID']
  end

  def getCurrentWeather(cityName)
    return HTTParty.get("http://api.openweathermap.org/data/2.5/weather?q=#{cityName}&appid=#{@appId}")
  end

end
